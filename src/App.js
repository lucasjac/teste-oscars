import FilmList from "./components/filmsList/index";
import Header from "./components/header/index";
import { useEffect, useState } from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "./App.scss";

function App() {
  const maxPage = 26;
  const [pageNumber, setPageNumber] = useState(1);
  const [nextUrl, setNextUrl] = useState(
    "https://api.themoviedb.org/3/movie/top_rated?api_key=c2458e3e9dd381b51836ea129c622699&language=en-US&page=" +
      pageNumber
  );
  const [filmsList, setFilmsList] = useState([]);
  const addMoviesToList = (list) => {
    if (pageNumber > 1 && pageNumber < 4) {
      setFilmsList([...filmsList]);
    } else {
      setFilmsList([...filmsList, ...list]);
    }
  };

  const filterMoviesList = (e) => {
    const { value } = e.target;
    if (value === "pop") {
      setPageNumber(1);
      setNextUrl(
        "https://api.themoviedb.org/3/movie/popular?api_key=c2458e3e9dd381b51836ea129c622699&language=en-US&page=" +
          pageNumber
      );
      setFilmsList([]);
    } else if (value === "now") {
      setPageNumber(1);
      setNextUrl(
        "https://api.themoviedb.org/3/movie/now_playing?api_key=c2458e3e9dd381b51836ea129c622699&language=en-US&page=" +
          pageNumber
      );
      setFilmsList([]);
    } else {
      setPageNumber(1);
      setFilmsList([]);
      setNextUrl(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=c2458e3e9dd381b51836ea129c622699&language=en-US&page=" +
          pageNumber
      );
    }
  };

  useEffect(() => {
    if (pageNumber < maxPage) {
      fetch(nextUrl)
        .then((Response) => Response.json())
        .then((Response) => {
          addMoviesToList(Response.results);
          setPageNumber(pageNumber + 1);
          setNextUrl(
            "https://api.themoviedb.org/3/movie/top_rated?api_key=c2458e3e9dd381b51836ea129c622699&language=en-US&page=" +
              pageNumber
          );
        });
    }
  }, [nextUrl, pageNumber]);

  return (
    <div className="App">
      <Header></Header>
      <select onClick={filterMoviesList} className="filters">
        <option style={{ display: "none" }}>Filtrar por</option>
        <option value="pop">Mais populares</option>
        <option value="score">Score do IMDB</option>
        <option value="now">Reproduzindo agora</option>
      </select>
      {pageNumber === maxPage ? (
        <FilmList filmsList={filmsList} setFilmsList={setFilmsList}></FilmList>
      ) : (
        <SkeletonTheme color="#999" highlightColor="#666">
          <p
            style={{
              margin: "0 auto",
              maxWidth: "40vw",
              columnCount: "2",
              columnFill: "auto",
            }}
          >
            <Skeleton
              count={6}
              style={{
                height: "45vh",
                width: "20vw",
                margin: "0 auto",
                position: "relative",
                left: "15%",
              }}
            />
          </p>
        </SkeletonTheme>
      )}
    </div>
  );
}

export default App;
