import "./style.scss";
import { useState } from "react";
import MovieDetails from "../movieDetails";

function Film({ film, index }) {
  const [movieDetails, setMovieDetails] = useState(false);
  const [favorites, setFavorites] = useState(true);

  return (
    <span>
      {index % 3 === 0 ? (
        <span
          onMouseLeave={(e) => {
            setMovieDetails(false);
          }}
          onMouseEnter={(e) => {
            setMovieDetails(true);
          }}
          style={{ display: "flex" }}
        >
          <img
            src={
              "https://www.themoviedb.org/t/p/w220_and_h330_face" +
              film.poster_path
            }
            alt={film.title}
            className="grande"
          ></img>
          {movieDetails ? (
            <MovieDetails film={film} favorite={favorites}></MovieDetails>
          ) : null}
        </span>
      ) : (
        <span
          onMouseLeave={(e) => {
            setMovieDetails(false);
          }}
          onMouseEnter={(e) => {
            setMovieDetails(true);
          }}
          style={{ display: "flex" }}
        >
          <img
            src={
              "https://www.themoviedb.org/t/p/w220_and_h330_face" +
              film.poster_path
            }
            alt={film.title}
            className="pequeno"
          ></img>
          {movieDetails ? <MovieDetails film={film}></MovieDetails> : null}
        </span>
      )}
    </span>
  );
}
export default Film;
