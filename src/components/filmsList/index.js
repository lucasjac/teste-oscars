import Film from "../film/index";
import "./style.scss";

function FilmsList({ filmsList }) {
  return (
    <div className="moviesList">
      {filmsList.map((film, index) => (
        <span key={index}>
          <Film film={film} index={index} />
        </span>
      ))}
    </div>
  );
}

export default FilmsList;
