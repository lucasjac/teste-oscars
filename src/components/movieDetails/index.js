import "./style.scss";
import { useState } from "react";

const MovieDetails = ({ film }) => {
  const [fav, setFav] = useState(true);
  const makeFavorite = () => {
    setFav(!fav);
  };
  return (
    <span className="movie-details">
      {fav ? (
        <button onClick={makeFavorite} className="favorite-button"></button>
      ) : (
        <button
          style={{ backgroundColor: "yellow" }}
          onClick={makeFavorite}
          className="favorite-button"
        ></button>
      )}

      <h4 className="movieTitle">{film.title}</h4>
      <h5 className="movieRelease">{film.release_date}</h5>
      <div className="linksConteiner">
        <a
          href={`https://www.youtube.com/results?search_query=${film.title}`}
          target="blank"
          style={{
            marginRight: "30%",
            textDecoration: "none",
            fontFamily: "Montserrat",
            fontSize: "1rem",
          }}
        >
          Trailer
        </a>

        <a
          href={`https://www.netflix.com/br/`}
          target="blank"
          style={{
            textDecoration: "none",
            fontFamily: "Montserrat",
            fontSize: "1rem",
          }}
        >
          Asistir
        </a>
      </div>
    </span>
  );
};
export default MovieDetails;
